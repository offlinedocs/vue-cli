# Build
FROM node:latest AS builder

RUN git clone https://github.com/vuejs/vue-cli.git /vue-cli

WORKDIR /vue-cli

RUN npm install
RUN npm run docs:build

# Serve
FROM nginx:alpine

COPY --from=builder /vue-cli/docs/.vuepress/dist /usr/share/nginx/html
